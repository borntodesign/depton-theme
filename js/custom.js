/**
* Custom Js
* @Depton Theme
*/


/*
==========================
  Registration Recaptcha
  Product Page Tempalate
==========================
*/

var verifyCallback = function(response) {
  $("html, body").animate({ scrollTop: $(".sect2").offset().top }, "slow");
  document.getElementById("message-my-alert").className = "recapt-hidden-msg";
};
var onloadCallback = function() {
  grecaptcha.render('my_re_captacha', {
    'sitekey' : '6LfTtVgUAAAAAC24XlZmePHXXZoTCTI_LOo4SCj9',
    'callback' : verifyCallback,
    'theme' : 'light'
  });
};

/*
===========================
  Miscellaneous Javascript
===========================
*/

(function($) {
  $(document).ready(function() {

    /****************** Responsive Menu ******************/
    $(document).on('click', '.navbar-toggler', function() {
      if($(this).attr('aria-expanded') == 'false') {
        $(this).attr('aria-expanded','true');
        $(this).removeClass('collapsed');

      }
      else {
        $(this).attr('aria-expanded','false');
        $(this).addClass('collapsed');
        $('#navbar-main-container').addClass('collapse');
      }
      $( "#navbar-main-container" ).toggle( "slow" );
    });

    /******* Register Link (Account page) *********/
    var register_html = '';
    register_html += '<div align="left" class="w-100">';
    register_html += 'Don’t have an account?&nbsp;<a href="http://depton.samtoms.tech/register/">Click here</a>';
    register_html += '</div>';
    $('.page-id-202 #wpmem_login form fieldset').append( register_html );

    /******************************
    *          validation         *
    *  ( Product page and popup ) *
    *******************************/

    /******* Username validation  *********/
    $('.sect1 .bnt').on('click', function(event) {
      event.preventDefault();
      var reg_user = $('#reg_user').val();
      if(isEmpty(reg_user)) {
        $('#message_alert').html( message_alert( 'Please enter a username' ) );
        return false;
      }
      else {
        $('#message_alert').empty();
        return true;
      }
    });

    /******* Email validation  *********/
    $('#reg_email').on('keypress', function() {
      var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(this.value);
      if(!re) {
        $('#message_alert').html( message_alert( 'Please enter a vaild email' ) );
      }
      else {
        $('#message_alert').empty();
        return true;
      }
    })

    /******* Phone validation  *********/
    $('.sect1 .bnt').on('click', function(event) {
      event.preventDefault();
      var phone = $('#reg_phone_number').val();
      if(isEmpty(phone)) {
        $('#message_alert').html( message_alert( 'Please enter a phone number' ) );
        return false;
      }
      else {
        $('#message_alert').empty();
        return true;
      }
    });

  });
})(jQuery);

/**
*  @package Depton Theme
*  @param { 'Please enter the message' }-message
*/

function message_alert( message ) {
  var message_html = ''
  message_html += '<div class="alert alert-dismissible alert-danger">';
  message_html += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
  message_html += '<strong>Sorry!  </strong>';
  message_html += message;
  message_html += '</div>';

  return message_html;
}

/**
*  @package Depton Theme
*  @param { string }-str
*/
function isEmpty(str) {
  return (!str || 0 === str.length);
}
