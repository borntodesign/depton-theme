/**
 * File ajax-custom.js.
 *
 * Ajax Custom enhancements for a better user experience.
 *
 * This File contains Register the user
 */

(function($) {
	$(document).ready(function(){

		$("#register_pro").on('click',function(e) {
      e.preventDefault();

	    var reg_user 					= $('#reg_user').val();
	    var reg_firstname 		= $('#reg_firstname').val();
	    var reg_company 			=	$('#reg_company').val();
	    var reg_phone_number	= $('#reg_phone_number').val();
	    var reg_country			  =	$('#reg_country').val();
	    var reg_email 				= $('#reg_email').val();
	    var reg_password 			= $('#reg_password').val();
			var reg_cnf_password 	= $('#reg_cnf_password').val();
			
			console.log("Send 1");

      if( reg_phone_number == '') {
        $('#message_alert').html( message_alert( 'Please enter a valid Number' ) );
        return false;
      }
      if( reg_country == '') {
        $('#message_alert').html(message_alert( 'Please enter country' ));
        return false;
      }
      if(reg_email == '' ) {
        $('#message_alert').html(message_alert( 'Please enter email address' ));
        return false;
      }
      if(  reg_password == '') {
        $('#message_alert').html(message_alert( 'Please enter a password' ));
        return false;
      }
			if(reg_password != reg_cnf_password) {
				$('#message_alert').html(message_alert( 'Passwords do not Match' ));
				return false;
			}
			else {
				$('#message_alert').empty();
			}
			console.log("Send 2");
			var $captcha = $( '#my_re_captacha' ),
			response = grecaptcha.getResponse();

			if (response.length === 0) {
				$( '.msg-alert').text( "reCAPTCHA is mandatory *" );
				$( '.msg-alert').addClass("message-error");
				if( !$captcha.hasClass( "error" ) ){
					$captcha.addClass( "error" );
				}
				//$(".recaptcha-container").append('<span class="msg-error error">reCAPTCHA is mandatory *</span>')
				return false;
			}
			else {
				$('#message_alert').empty();
			}

	    var form_value = {
	      'username' : reg_user,
	      'firstname': reg_firstname,
	      'company' : reg_company,
				'country' : reg_country,
	      'phone_number' : reg_phone_number,
	      'email': reg_email,
	      'password': reg_password,
	    }

			var dataform = {
				'action': 'product_registeration',
				'form_value': form_value
			};

			$.ajax({
				url: ajaxurl,
				method: 'POST',
				data: dataform,
				success: function(data){
				  $('#message_alert').html(data);
				},
				error: function(data){
				  $('#message_alert').html(data);
				}
			});

		});

	});

	// See Full Range Button
	$('.sect2').fadeOut('fast');
	$(document).ready(function() {
		$('a.fadebtn').on('click', function(event) {
			event.preventDefault();
			if($('.sect1').children('.fst').val()) {
				$(this).parent('.sect1').fadeOut('fast');
				$(this).parent('.sect1').siblings('.sect2').fadeIn('fast');
			}
			else{
				//jQuery(this).parents('.sect1').siblings('.sect2').children('button').trigger('click');
			}
		});

	// 3 videos about page
	$(document).ready(function() {
		$('.all_video_url').on("click",function(){
			var youtube_id =	$(this).attr('data-youtube_id');
			var src = "https://www.youtube.com/embed/"+youtube_id
		 	$(".three_video iframe").attr('src',src);
		});
	});

	$(document).ready(function() {
		$('.videoModal').on('hide.bs.modal', function(e) {
			var $if = $(e.delegateTarget).find('iframe');
			var src = $if.attr("src");
			$if.attr("src", '/empty.html');
			$if.attr("src", src);
		});
	});

	$(document).ready(function(){
		$('#ar').click(function() {
			 // event.preventDefault();
			 	$('.WS').animate({
				 scrollLeft: "+=500px"
			 }, "slow");
		});
		$('#al').click(function() {
			// event.preventDefault();
			$('.WS').animate({
				scrollLeft: "-=500px"
			}, "slow");
		});
	});

	});

})(jQuery);
