<?php
//
// Template Name: Products
//
?>
<?php get_header(); ?>
<?php $product_header_banner = get_field('product_header_image'); ?>
<?php if( $product_header_banner['product_background_image'] ) :?>
	<section class="hero py-5 px-2" style="background-image: url(<?= $product_header_banner['product_background_image']; ?>)" >
    <div class="container py-5">
      <div class="row py-5">
        <div class="col-12  py-5">
          <div class="py-lg-5">
						<?php	if( $product_header_banner['banner_text'] ) : ?>
						<h1 class="text-uppercase font-weight-bold text-white"><?= $product_header_banner['banner_text']; ?></h1>
						<?php endif ?>
						<?php	if( $product_header_banner['product__banner_button']['title'] ) : ?>
						<a href="#register" class="btn text-white d-flex justify-content-between d-block px-3 px-md-4 py-3 mt-5 QS">
              <span><?= $product_header_banner['product__banner_button']['title']; ?></span>
              <span><img src="<?= get_template_directory_uri( ); ?>/img/btn-arrow.png" alt=""></span>
						</a>
						<?php endif ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif ?>

<?php $section_a = get_field('section_a'); ?>
<section class="FTR bgb py-2">
  <div class="container py-5">
    <div class="row text-white ">
      <div class="col-md-6 py-3 pr-md-5">
        <div class="row">
          <div class="col-1">
            <img src="<?= get_template_directory_uri( ); ?>/img/octagon.png" alt="">
          </div>
          <div class="col-10">
            <h4 class="font-weight-bold text-uppercase">#OEM sourcing</h4>
            <p class="QS fz14"><?= ($section_a['oem_sourcing']) ? $section_a['oem_sourcing'] : ''; ?></p>
          </div>
        </div>
      </div>
      <div class="col-md-6 py-3 pr-md-5">
        <div class="row">
          <div class="col-1">
            <img src="<?= get_template_directory_uri( ); ?>/img/octagon.png" alt="">
          </div>
          <div class="col-10">
            <h4 class="font-weight-bold text-uppercase">#ODM sourcing</h4>
            <p class="QS fz14"><?= ($section_a['odm_sourcing']) ? $section_a['odm_sourcing'] : ''; ?></p>
          </div>
        </div>
      </div>
    </div>
    <div class="row text-white ">
      <div class="col-md-6 py-3 pr-md-5">
        <div class="row">
          <div class="col-1">
            <img src="<?= get_template_directory_uri(); ?>/img/octagon.png" alt="">
          </div>
          <div class="col-10">
            <h4 class="font-weight-bold text-uppercase">#shipping</h4>
            <p class="QS fz14"><?= ($section_a['shipping']) ? $section_a['shipping'] : ''; ?></p>
          </div>
        </div>
      </div>
      <div class="col-md-6 py-3 pr-md-5">
        <div class="row">
          <div class="col-1">
            <img src="<?= get_template_directory_uri( ); ?>/img/octagon.png" alt="">
          </div>
          <div class="col-10">
            <h4 class="font-weight-bold text-uppercase">#customs clearance</h4>
            <p class="QS fz14"><?= ($section_a['customs_clearance']) ? $section_a['customs_clearance'] : ''; ?></p>
          </div>
        </div>
      </div>
    </div>
    <div class="row text-white ">
      <div class="col-md-6 py-3 pr-md-5">
        <div class="row">
          <div class="col-1">
            <img src="<?= get_template_directory_uri( ); ?>/img/octagon.png" alt="">
          </div>
          <div class="col-10">
            <h4 class="font-weight-bold text-uppercase">#Design</h4>
            <p class="QS fz14"><?= ($section_a['design']) ? $section_a['design'] : ''; ?></p>
          </div>
        </div>
      </div>
      <div class="col-md-6 py-3 pr-md-5">
        <div class="row">
          <div class="col-1">
            <img src="<?= get_template_directory_uri( ); ?>/img/octagon.png" alt="">
          </div>
          <div class="col-10">
            <h4 class="font-weight-bold text-uppercase">#Project management</h4>
            <p class="QS fz14"><?= ($section_a['project_management']) ? $section_a['project_management'] : ''; ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php $ethics = get_field('ethics'); ?>
<section class="ETH py-5">
  <div class="container py-5">
    <h1 class="fwl tilt text-uppercase">ETHICS</h1>
    <div class="row ">
      <div class="col-12">
        <p class=" cl afbf text-uppercase m-0">Products</p>
        <h2 class="cb font-weight-bold text-uppercase"><?= ($ethics['title']) ? $ethics['title'] : ''; ?></h2>
        <p class="mt-3"><?= ($ethics['description']) ? $ethics['description'] : ''; ?></p>
			</div>
    </div>
    <div class="row">
      <div class="col-md-4 py-4 pr-md-5 pl-md-4">
        <img src="<?= get_template_directory_uri( ); ?>/img/e1.png" alt="">
        <h3 class="fwb text-uppercase mt-4">Safety</h3>
        <p class="mt-3 QS  "><?= ($ethics['safety']) ? $ethics['safety'] : ''; ?></p>
      </div>
      <div class="col-md-4 py-4 pr-md-5 pl-md-4">
        <img src="<?= get_template_directory_uri( ); ?>/img/e2.png" alt="">
        <h3 class="fwb text-uppercase mt-4">living wages</h3>
        <p class="mt-3 QS  "><?= ($ethics['living_wages']) ? $ethics['living_wages'] : ''; ?></p>
      </div>
      <div class="col-md-4 py-4 pr-md-5 pl-md-4">
        <img src="<?= get_template_directory_uri( ); ?>/img/e3.png" alt="">
        <h3 class="fwb text-uppercase mt-4">future</h3>
        <p class="mt-3 QS  "><?= ($ethics['future']) ? $ethics['future'] : ''; ?></p>
      </div>
    </div>
  </div>
</section>

<?php $design = get_field('design'); ?>
<section class="TR pb-5">
  <div class="container py-5">
    <h1 class="fwl tilt text-uppercase">Design</h1>
    <div class="row pb-5">
      <div class="col-md-6">
				<?php if( $design['left_side']['slider'] > 0 ):	?>
        <div id="carousel" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner product-caro">
						<?php $count = 0 ;?>
						<?php foreach( $design['left_side']['slider'] as $slide_src ) : ?>
            <div class="carousel-item px-md-5 <?php echo ($count == 0) ? 'active' : ''; ?>">
              <img class="d-block w-100" src="<?= $slide_src['slide'] ?>" alt="">
            </div>
						<?php $count++ ;?>
						<?php endforeach; ?>
          </div>
          <div class="indi d-flex justify-content-end">
            <a class="carousel-control-prev cip px-1" href="#carousel" role="button" data-slide="prev">
              <img src="<?= get_template_directory_uri( ); ?>/img/arrow-left.png" class="w-100" alt="">
            </a>
            <a class="carousel-control-next cin px-1" href="#carousel" role="button" data-slide="next">
              <img src="<?= get_template_directory_uri( ); ?>/img/arrow-right.png" class="w-100" alt="">
            </a>
          </div>
        </div>
			<?php else : ?>
			<?php // no rows found ?>
			<?php  endif; ?>
      </div>
      <div class="col-md-6 d-flex product-slider-text">
				<?php if( $design['right_side']['text_slider'] > 0 ):	?>
        <div id="carousel" class="carousel  txtcrl carousel-fade d-flex" data-ride="carousel">
          <div class="carousel-inner d-flex flex-column">
						<?php $count_trainer = 0 ;?>
						<?php foreach( $design['right_side']['text_slider'] as $trainer) : ?>
	            <div class="carousel-item px-md-5 h-100 <?= ($count_trainer == 0) ? 'active' : ''; ?> ">
	              <div class="txt d-flex flex-column justify-content-center  h-100">
	                <h3 class="fwb cb text-uppercase phone-numbe-trainer"><?= $trainer['id'] ?></h3>
	                <h1 class="fwb cb text-uppercase"><?= $trainer['title'] ?></h1>
	                <p class="QS mt-3">
									<?= $trainer['description'] ?>
	                </p>
	              </div>
	            </div>
						<?php $count_trainer++ ;?>
						<?php endforeach; ?>
          </div>
        </div>
				<?php else : ?>
				<?php // no rows found ?>
				<?php  endif; ?>
      </div>
    </div>
  </div>
</section>

<section class="FRM py-5" id="register">
  <div class="container py-4">
    <div class="row">
      <div class="col-12 text-center">
        <p class=" cl afbf text-uppercase m-0">Products</p>
        <h1 class="cb font-weight-bold text-uppercase">view the full range</h1>
      </div>
    </div>
    <div class="row justify-content-center d-flex pb-5">
      <div class=" col-sm-10 col-md-6">
				<div id="message_alert"></div>
        <form action="" method="get" class="form-group">
          <div class="sect1">
            <input type="text" id="reg_user" name="username" class="form-control fwb my-4 pl-4 py-2 cb  fst" placeholder="USERNAME*" required="">
            <input type="text" id="reg_firstname" name="firstname" class="form-control fwb my-4 pl-4 py-2 cb" placeholder="FIRST NAME">
            <input type="text" id="reg_company" name="company" class="form-control fwb my-4 pl-4 py-2 cb" placeholder="COMPANY">
            <a href="#" class="bnt py-3 px-4 text-white justify-content-between d-flex mt-4 fadebtn">
              <span>See full range</span>
              <span><img src="<?= get_template_directory_uri( ); ?>/img/btn-arrow.png" alt=""></span>
            </a>

          </div>
          <div class="sect2" >
            <input type="text" id="reg_phone_number" name="phone_number" class="form-control fwb my-4 pl-4 py-2 cb" placeholder="PHONE NUMBER*" required="">
            <input type="text" id="reg_country" name="country" class="form-control fwb my-4 pl-4 py-2 cb" placeholder="COUNTRY*" required="">
            <input type="email" id="reg_email"  name="email" class="form-control fwb my-4 pl-4 py-2 cb" placeholder="E-MAIL ADDRESS*" required="">
            <input type="password" id="reg_password" name="password" class="form-control fwb my-4 pl-4 py-2 cb" placeholder="PASSWORD" required="">
            <input type="password" id="reg_cnf_password" name="password" class="form-control fwb my-4 pl-4 py-2 cb" placeholder="CONFIRM PASSWORD*" required="">
            <button type="submit" class="bnt py-3 px-4 text-white justify-content-between d-flex mt-4" id="register_pro" name="register">
              <span>Finish Registration</span>
              <span><img src="<?= get_template_directory_uri( ); ?>/img/btn-arrow.png" alt=""></span>
            </button>
            <div class="recaptcha-container">
						  <div id="my_re_captacha"></div>
						  <span class="msg-alert" id = "message-my-alert"></span>
				  	</div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>
