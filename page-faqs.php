<?php
//
// Template Name: Faqs
//

get_header();
?>
	<section class="SM py-5">
    	<div class="container">
				<!-- Start FAQ Block -->
				<?php
				// check if the repeater field has rows of data
				if( have_rows('faq') ):
						// loop through the rows of data
						while ( have_rows('faq') ) : the_row();
						?>
						<div class="row pb-3">
							<div class="col-12">
								<p class=" cl afbf text-uppercase m-0">FAQS</p>
								<h2 class="cb font-weight-bold text-uppercase"><?php the_sub_field('block_name'); ?></h2>
							</div>
						</div>
						<div class="row">
							<div class="col-10">
							<?php

								// check if the repeater field has rows of data
								if( have_rows('faqs') ):
									$it = 0;
									// loop through the rows of data
										while ( have_rows('faqs') ) : the_row();
										?>
											<div class="pbox mb-3">
												<div class="head text-white d-flex justify-content-between px-4 py-4">
													<p class=" QS m-0"><?php the_sub_field('name'); ?></p>
													<i class="h fa fa-plus down mt-1" ></i>
													<i class="h fa fa-minus up sr-only mt-1" ></i>
												</div>
												<div class="box pt-5 pb-2 pl-2" >
													<p><?php the_sub_field('answer'); ?></p>
												</div>
											</div>
										<?php
										endwhile;

								else :

										// no rows found

								endif;

								?>
							</div>
						</div>
						<?php
						endwhile;

				else :

						// no rows found

				endif;

				?>
				<!-- End FAQ block -->
    	</div>
    </section>
    <section class="AF">
      <div class="container pt-2">
        <div class="row text-center pt-5 pb-4">
          <div class="col-12 py-5">
            <h4 class="text-uppercase fwm">Start a FREE consultation with us today. <b>No obligations.</b></h4>
            <h4 class="fwm">We would love to have a chat with you about shoes.</h4>
            <br>
            <div class="lnk d-md-flex justify-content-center">
              <p class=" m-0 d-flex justify-content-center flex-column px-4 fwm RD">+86 28 8777 9435</p>
              <p class="fa-3x cb m-0 d-none d-md-block">/</p>
              <p class=" m-0 d-flex justify-content-center flex-column px-4 fwm QS"><a href="#">sales@depton.com.cn</a></p>
            </div>
          </div>
        </div>
      </div>
    </section>
		<script style="font-size: 16px;">
      $( document ).ready(function() {
        $('.box').hide();
        // $('.quest > p').hide();
        
        $('.head').click(function(event) {
          console.log('clicked');
          event.preventDefault();
          $(this).toggleClass('active');
          $(this).children('.h.fa.up').toggleClass('sr-only');;
          $(this).children('.h.fa.down').toggleClass('sr-only');;
          $(this).siblings('.box').toggle('fast');
        });
      });
    </script>
<?php
get_footer();
