<svg width="120" height="110" viewBox="0 0 120 110" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g id="Canvas" fill="none"><g id="image"><mask id="mask<?php echo $SVGNo; ?>" mask-type="alpha" maskUnits="userSpaceOnUse" x="-1" y="-1" width="122" height="111"><g id="Vector 2"><path d="M 30.9464 0L 0 54.2335L 30.9464 110.017L 91.2919 110.017L 120.691 54.2335L 91.2919 0L 30.9464 0Z" transform="translate(-0.254028 -0.0167847)" fill="#C4C4C4"/></g></mask><g mask="url(#mask0)"><g id="image 3"><rect width="120" height="110" fill="url(#pattern<?php echo $SVGNo; ?>)"/></g></g></g></g>
<defs>
    <pattern id="pattern<?php echo $SVGNo; ?>" patternContentUnits="objectBoundingBox" width="1" height="1">
    <use xlink:href="#image<?php echo $SVGNo; ?>" transform="translate(-0.308824 0) scale(0.00359477 0.00392157)"/>
    </pattern>
    <image id="image<?php echo $SVGNo; ?>" data-name="image.png" width="450" height="255" xlink:href="<?php echo wp_get_attachment_image_url( $imageSVG, 'home-slider' ); ?>"/>
</defs>
</svg>