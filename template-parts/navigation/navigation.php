<?php
/**
 * Displays  navigation
 *
 * @package WordPress
 * @subpackage Dipton Theme
 * @version 0.01
 */

wp_nav_menu( array(
  'theme_location' => 'primary',
  'container_class' => 'collapse navbar-collapse justify-content-end mt-5 text-white py-3 btw fwb',
  'container_id'    => "navbar-main-container",
  'menu_id'        => 'nav-menu',
  'menu_class'=> 'navbar-nav',
  'items_wrap' => my_nav_wrap()
));
?>
