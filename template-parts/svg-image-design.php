<svg viewBox="0 0 569 448" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<g id="Canvas" fill="none">
<g id="Group 5">
<g id="Group 3">
<mask id="mask<?php echo $SVGNo_first; ?>" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="313" height="285">
<path id="Vector 2.8" d="M 80.2564 0L 0 140.493L 80.2564 285L 236.756 285L 313 140.493L 236.756 0L 80.2564 0Z" fill="#C4C4C4"/>
</mask>
<g mask="url(#mask<?php echo $SVGNo_first; ?>)">
<rect id="image 2" width="313" height="285" transform="translate(0 -1)" fill="url(#pattern<?php echo $SVGNo_first; ?>)"/>
</g>
</g>
<g id="Group 3_2">
<mask id="mask<?php echo $SVGNo_second; ?>" mask-type="alpha" maskUnits="userSpaceOnUse" x="256" y="163" width="313" height="285">
<path id="Vector 2.11" d="M 80.2564 0L 0 140.493L 80.2564 285L 236.756 285L 313 140.493L 236.756 0L 80.2564 0Z" transform="translate(256 163)" fill="#C4C4C4"/>
</mask>
<g mask="url(#mask<?php echo $SVGNo_second; ?>)">
<rect id="image 2_2" width="311" height="285" transform="translate(258 163)" fill="url(#pattern<?php echo $SVGNo_second; ?>)"/>
</g>
</g>
</g>
</g>
<defs>
<pattern id="pattern<?php echo $SVGNo_first; ?>" patternContentUnits="objectBoundingBox" width="1" height="1">
<use xlink:href="#image<?php echo $SVGNo_first; ?>" transform="translate(-0.15655 -0.0350877) scale(0.00319489 0.00350877)"/>
</pattern>
<pattern id="pattern<?php echo $SVGNo_second; ?>" patternContentUnits="objectBoundingBox" width="1" height="1">
<use xlink:href="#image<?php echo $SVGNo_second; ?>" transform="translate(-0.2 0) scale(0.00319489 0.00350877)"/>
</pattern>
<image id="image<?php echo $SVGNo_first; ?>" data-name="image.png" width="450" height="320" xlink:href="<?php echo wp_get_attachment_image_url( $imageSVG, 'home-slider' ); ?>"/>
<image id="image<?php echo $SVGNo_second; ?>" data-name="image.png" width="450" height="320" xlink:href="<?php echo wp_get_attachment_image_url( $imageSVG2, 'home-slider' ); ?>"/>
</defs>
</svg>