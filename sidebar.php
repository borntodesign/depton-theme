<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package depton
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>
<!--
<aside id="secondary" class="widget-area">
	<?php // dynamic_sidebar( 'sidebar-1' ); ?>
</aside>
-->
<div class="col-md-4 pl-md-5 px-3 side">
	<div class="plugin mt-5">
	<p class="text-uppercase m-0 mt-4 n"><span class="cl afbf">Blog</span></p>
	<h2 class="cb font-weight-bold text-uppercase">Blog post title</h2>
	<ul class="list-unstyled mt-4">
		<?php

		global $post;
		$args = array( 'posts_per_page' => 5 );

		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
			<li class="mb-1">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</li>
		<?php endforeach; 
		wp_reset_postdata();?>
	</ul>
	<br>
	</div>
	<div class="plugin">
	<p class="text-uppercase m-0 mt-4 n"><span class="cl afbf">Blog</span></p>
	<h2 class="cb font-weight-bold text-uppercase">Tags</h2>
	<ul class="list-unstyled list-inline mt-4">
		<li class=" list-inline-item text-uppercase fwm fz14  btns">
		<?php
		$posttags = get_tags(array(
			'hide_empty' => false
		  ));
		if ($posttags) {
		foreach($posttags as $tag) {
			echo '<a href="' . $tag->slug . '" class="bgb mr-2 mb-2 d-inline-block tdn text-white px-3 py-2">' . $tag->name . '</a>'; 
		}
		}
		?>
		</li>
	</ul>
	<br>
	</div>
</div>