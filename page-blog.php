<?php
//
// Template Name: Blog
//

get_header();
?>
	<section class="SM py-5">
        <div class="container py-5">
            <div class="row">
                <div class="col-md-8 pr-md-5 px-3">

										<?php
										$args = array(
												'post_type'      => 'post',
												'posts_per_page' => -1
										);
										
										$query = new WP_Query($args);
                    if ( $query->have_posts() ) :

                        /* Start the Loop */
                        while ( $query->have_posts() ) :
														$query->the_post();

                            /*
                            * Include the Post-Type-specific template for the content.
                            * If you want to override this in a child theme, then include a file
                            * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                            */
                            get_template_part( 'template-parts/content', 'content' );

                        endwhile;

                    endif;
                    ?>

                </div>
                <?php get_sidebar(); ?>
            </div>
        </div>
    </section>

<?php
get_footer();