<?php
/**
 * depton functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package depton
 */

if ( ! function_exists( 'depton_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function depton_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on depton, use a find and replace
		 * to change 'depton' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'depton', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'depton' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'depton_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'depton_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function depton_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'depton_content_width', 640 );
}
add_action( 'after_setup_theme', 'depton_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function depton_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'depton' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'depton' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'depton_widgets_init' );

/**
 * Enqueue scripts and styles.
 */

function depton_scripts() {

	wp_enqueue_style( 'depton-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' );
	wp_enqueue_style( 'depton-fonts', 'https://fonts.googleapis.com/css?family=Quicksand:400,500,700|Rajdhani:400,500,700' );
	wp_enqueue_style( 'depton-style', get_stylesheet_uri() );
	/* Site styles from HTML conversion */
	wp_enqueue_style( 'depton-common-style', get_template_directory_uri() . '/layouts/common.css', null, null );
	wp_enqueue_style( 'depton-blog-style', get_template_directory_uri() . '/layouts/blog.css' );
	wp_enqueue_style( 'depton-content-style', get_template_directory_uri() . '/layouts/content-sidebar.css' );
	wp_enqueue_style( 'depton-sidebar-style', get_template_directory_uri() . '/layouts/sidebar-content.css' );
	wp_enqueue_style( 'depton-single-style', get_template_directory_uri() . '/layouts/single-blog.css' );

	/* Page specific styles */
	if ( get_page_template_slug() == 'page-about.php' ) {
        wp_enqueue_style( 'depton-startups-style', get_template_directory_uri() . '/layouts/about.css', null, null );
	}
	if ( get_page_template_slug() == 'page-home.php' ) {
		wp_enqueue_style( 'depton-homepage-style', get_template_directory_uri() . '/layouts/homepage.css', null, null  );
		wp_enqueue_script( 'depton-scroll-icons', get_template_directory_uri() . '/js/scrollreveal.min.js', null, null, false );
		wp_enqueue_script( 'depton-scroll', 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/2542/jquery.scrollie.min_1.js', null, null, true );
		wp_enqueue_script( 'depton-waypoints', 'https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.0/jquery.waypoints.min.js', null, null, true );
		wp_enqueue_script( 'depton-scroll-numbers', get_template_directory_uri() . '/js/jquery.counterup.min.js', null, null, true );
	}
	if ( get_page_template_slug() == 'page-startups.php' ) {
        wp_enqueue_style( 'depton-startups-style', get_template_directory_uri() . '/layouts/start-ups.css' );
	}
	if ( get_page_template_slug() == 'page-products.php' ) {
        wp_enqueue_style( 'depton-product-style', get_template_directory_uri() . '/layouts/product.css', null, null );
	}
	if ( get_page_template_slug() == 'page-faqs.php' ) {
        wp_enqueue_style( 'depton-faq-style', get_template_directory_uri() . '/layouts/faq.css' );
	}
	if ( get_page_template_slug() == 'page-contact.php' ) {
        wp_enqueue_style( 'depton-contact-style', get_template_directory_uri() . '/layouts/contact.css', null, null  );
	}

	wp_enqueue_script( 'depton-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'depton-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'depton_scripts' );

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
* Register Three widget areas.
*
* @since Depton Theme
*
* @return void
*/
if ( ! function_exists( 'my_awesome_function' ) ) {

	function depton_widgets_init_footer() {
		register_sidebar( array(
	    'name'          => __( 'Footer Section Column A', 'depton' ),
	    'id'            => 'footer-1',
	    'description'   => __( 'Appears in the footer section of the site.', 'depton' ),
	    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	    'after_widget'  => '</aside>',
	    'before_title'  => '<h3 class="widget-title">',
	    'after_title'   => '</h3>',
		) );

		register_sidebar( array(
	    'name'          => __( 'Footer Section Column B', 'depton' ),
	    'id'            => 'footer-2',
	    'description'   => __( 'Appears in the footer section of the site.', 'depton' ),
	    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	    'after_widget'  => '</aside>',
	    'before_title'  => '<h3 class="widget-title">',
	    'after_title'   => '</h3>',
		) );
		register_sidebar( array(
			'name'          => __( 'Footer Section Column C', 'depton' ),
			'id'            => 'footer-3',
			'description'   => __( 'Appears in the footer section of the site.', 'depton' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
		register_sidebar( array(
			'name'          => __( 'Footer Section Column D', 'depton' ),
			'id'            => 'footer-4',
			'description'   => __( 'Appears in the footer section of the site.', 'depton' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	}
}
add_action( 'widgets_init', 'depton_widgets_init_footer' );
add_image_size( 'about-thumb', 240, 240, true ); //
add_image_size( 'home-slider', 450, 320, true );

/**
* Add class in li and link(a) tags
* @package depton
*/
add_filter ( 'nav_menu_css_class', 'add_menu_item_class', 10, 4 );
add_filter( 'nav_menu_link_attributes', 'lnk_menu_add_class', 10, 3 );

function add_menu_item_class ( $classes, $item, $args, $depth ) {
  $classes[] = 'nav-item';
  return $classes;
}
function lnk_menu_add_class( $atts, $item, $args ) {
  $class = 'nav-link'; // or something based on $item
  $atts['class'] = $class;
  return $atts;
}

/**
* Get YouTube Video ID
* @package depton
*/
function getYouTubeID($video_link) {
	$pattern = '#(?<=(?:v|i)=)[a-zA-Z0-9-]+(?=&)|(?<=(?:v|i)\/)[^&\n]+|(?<=embed\/)[^"&\n]+|(?<=‌​(?:v|i)=)[^&\n]+|(?<=youtu.be\/)[^&\n]+#';
	preg_match($pattern, $video_link, $matches);
	$url = reset($matches);
	$url = strtok($url, '?');
	return $url;
}

/**
* Added Admin url in wp_head()
* @package depton-theme
*/
add_action('wp_head', 'admin_ajaxurl');
function admin_ajaxurl() {
  echo '<script type="text/javascript">
      		var ajaxurl = "' . admin_url('admin-ajax.php') . '";
      	</script>';
}

/**
* Product Page Registration
* @package depton-theme
*/
add_action( 'wp_ajax_nopriv_product_registeration', 'product_register_form' );
add_action( 'wp_ajax_product_registeration', 'product_register_form' );

function product_register_form() {
	global $wpdb;
	$user_info =  $_POST['form_value'] ;
	$firstname = ($user_info['firstname']) ? 'firstname' : '';
	$company = ($user_info['company']) ? 'company' : '';
	$user_id = username_exists( $user_info['username'] );
	if ( !$user_id and email_exists($user_info['email']) == false ) {
		$hash_password = wp_hash_password($user_info['password']);
		$user_id = wp_create_user( $user_info['username'], $hash_password, $user_info['email'] );
		add_user_meta($user_id,'first_name',$firstname);
		add_user_meta($user_id,'company',$company);
		add_user_meta($user_id,'country',$user_info['country']);
		add_user_meta($user_id,'phone_number',$user_info['phone_number']);
		$message	=  '<div class="alert alert-dismissible alert-success">';
		$message .= '<button type="button" class="close" data-dismiss="alert">&times;</button>';
		$message .= '<strong>Well done!</strong> Thanks for registration.';
		$message .= '</div>';
		$result['message']=$message;
	}
	else {
		$message  = '<div class="alert alert-dismissible alert-danger">';
		$message .= '<button type="button" class="close" data-dismiss="alert">&times;</button>';
		$message .= '<strong>Sorry!</strong> <a href="#" class="alert-link">User already exists.';
		$message .= '</div>';
		$result['message']=$message;
	}
	echo $message;
	wp_die(); // this is required to terminate immediately and return a proper response
}


/**
* Svg upload images
* @package depton-theme
*/
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function fix_svg_thumb_display() {
  echo '<style>
    td.media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail {
      width: 100% !important;
      height: auto !important;
    }
		</style>
  ';
}
add_action('admin_head', 'fix_svg_thumb_display');

/**
* Wp Member Plugin
* Changes in Login Form
* @package depton-theme
*/
function my_login_args( $args ) {
  $args = array( 'heading'  => "Depton Login" );
  return $args;
}
add_filter( 'wpmem_inc_login_args', 'my_login_args' );

function my_login_form_args( $args, $action ) {
  $args = array(
		'heading_after'   => '</legend><hr>',
    'row_before' 			=> '<div class="form-group">',
    'row_after'  			=> '</div>',
		'link_before'     => '<div align="left" class="w-100">',
		'link_after'      => '</div>',
		'buttons_before'  => '<div class="form-group">',
		'buttons_after'   => '</div>',
		'button_class'    => 'btn btn-primary w-100',
  );

  return $args;
}
add_filter( 'wpmem_login_form_args', 'my_login_form_args', 10, 2 );

function my_login_inputs( $array ) {
	$array = array(
    array(
      'name'   => __( 'Email or Username :' ),
      'type'   => 'text',
      'tag'    => 'log',
      'class'  => 'form-control',
      'div'    => 'form_fields'
    ),
    array(
      'name'   => __( 'Password :' ),
      'type'   => 'password',
      'tag'    => 'pwd',
      'class'  => 'form-control',
      'div'    => 'form_fields'
    )
  );

  return $array;
}
add_filter( 'wpmem_inc_login_inputs', 'my_login_inputs' );

function my_register_form_args( $args, $toggle ) {

  $args = array(
		// wrappers
    'heading_before'   => '<legend class="w-100 asd">',
    'heading_after'    => '</legend>',
    'row_before' 			 => '<div class="w-100 float-left">',
    'row_after' 			 => '</div>',
		'buttons_before'   => '<div class="float-right button_container">',
    'buttons_after'    => '</div>',
		'main_div_before'  => '<div id="wpmem_reg">',
 		'main_div_after'   => '</div>',
		// classes & ids
	 	'button_id'        => '',
	 	'button_class'     => 'btn btn-success',
	 	// buttons
    'submit_update'    => __( 'Update Profile', 'wp-members' ),
  );

  return $args;
}
add_filter( 'wpmem_register_form_args', 'my_register_form_args', 10, 2 );

function my_msg_dialog( $args, $toggle ) {
    $args['div_before'] = '<div class="alert alert-success alert-msg">';
    $args['p_before'] = '';
    $args['p_after']  = '';
    if ( $toggle == 'success' ) {
      $args['div_before'] = '<div class="my_success_class">';
    }

    return $args;
}
add_filter( 'wpmem_msg_dialog_arr', 'my_msg_dialog', 10, 2 );

/**
* Logout Redirect
* Logout redirect to login page.
* @package depton-theme
*/
function my_logout_redirect( $redirect_to ) {
	$url = site_url( '/login/');

	return $url;
}
add_filter( 'wpmem_logout_redirect', 'my_logout_redirect' );

/**
* Remove Admin Bar
* if user is login except admin
* @package depton-theme
*/
function remove_admin_bar() {
	// if (!current_user_can('administrator') && !is_admin()) {
	//   show_admin_bar(false);
	// }
	show_admin_bar(false);
}
add_action('after_setup_theme', 'remove_admin_bar');

/**
* Language Switcher
* English(EN) and Russian(RU)
* @package depton-theme
*/
function my_nav_wrap() {
  // default value of 'items_wrap' is <ul id="%1$s" class="%2$s">%3$s</ul>'

  // open the <ul>, set 'menu_class' and 'menu_id' values
  $wrap  = '<ul id="%1$s" class="%2$s">';

  // get nav items as configured in /wp-admin/
  $wrap .= '%3$s';

  // the static link
  $wrap .= '<li class="nav-item pl-3" ><a class="nav-link d-inline-block pr-0 cl" href="?lang=de">RU</a>
  <a class="nav-link d-inline-block pl-0" href="?lang=en">EN</a></li>';

  // close the <ul>
  $wrap .= '</ul>';
  // return the result
  return $wrap;
}
/**
* Footer-wp-member
* Added footer-wp-member class in body for footer issue
* @package depton-theme
*/
function custom_class( $classes ) {
  if ( is_page_template( 'page-wp-member.php' ) ) {
    $classes[] = 'footer-wp-member';
  }
  return $classes;
}
add_filter( 'body_class', 'custom_class' );

function wpmem_default_text_strings($text) {
	$text = array(
    'register_link_before' => __( 'Don’t have an account?', 'wp-members' ) . '&nbsp;',
    'register_link'        => __( 'Click here', 'wp-members' ),
	);
	return $text;
}
add_filter( 'wpmem_default_text_strings', 'wpmem_default_text_strings' );
