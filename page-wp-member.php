<?php
//
// Template Name: Wp Members
//
/**
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package depton
 */
?>
<?php get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="container">
		<?php	while ( have_posts() ) : ?>
			<?php the_post();
			get_template_part( 'template-parts/content', 'members' ); ?>
		<?php endwhile;	?>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
