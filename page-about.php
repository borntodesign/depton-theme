<?php
//
// Template Name: About
//

get_header();
?>

	<div class="hero pt-5">
      <div class="container py-5">
        <div class="col-12 text-center">
          <h6 class=" cl afbf text-uppercase">About us</h6>
          <h1 class="cb font-weight-bold text-uppercase h1">Our mission &amp; values</h1>
          <div class="py-5">
						<div class="hexgon_container">
              
							<div class="full_hexgon_about">
                <div class="hexgon_arrow"></div>
								<!-- Start Top -->
								<div id="top_hexgen_about" class="common_hexgen_abot">
									<img src="<?= get_template_directory_uri( ); ?>/img/svg/about_shape_top.svg"/>
								</div>
								<!-- end Top -->

								<!-- Start center -->
								<div id="center_hexgen_about" class="common_hexgen_abot" data-toggle="modal" data-target="#modal">
									<img src="<?= get_template_directory_uri( ); ?>/img/svg/about_logo-square.svg"/>
								</div>

								<!-- Start Bottom -->
								<div id="bottom_hexgen_about" class="common_hexgen_abot">
									<img src="<?= get_template_directory_uri( ); ?>/img/svg/about_shape_bottom.svg"/>
								</div>
								<!-- Start left -->
								<div id="left_hexgen_about" class="common_hexgen_abot">
									<img src="<?= get_template_directory_uri( ); ?>/img/svg/about_shape_left.svg"/>
								</div>
								<!-- Start right -->
								<div id="right_hexgen_about" class="common_hexgen_abot">
									<img src="<?= get_template_directory_uri( ); ?>/img/svg/about_shape_right.svg"/>
								</div>
							</div>
						</div>
          </div>
        </div>
      </div>
    </div>
    <section class="FTR pb-5">
      <div class="container">
        <div class="row">
          <div class="col-md-3 px-md-3 ">
            <h2 class="cb font-weight-bold text-uppercase m-0">GROWTH</h2>
            <p class="fz12 cl afbf text-uppercase m-0">About us</p>
            <p class=" QS mt-2 fz14"><?= the_field('growth'); ?></p>
          </div>
          <div class="col-md-3 px-md-3 ">
            <h2 class="cb font-weight-bold text-uppercase m-0">RELIABILITY</h2>
            <p class="fz12 cl afbf text-uppercase m-0">About us</p>
            <p class=" QS mt-2 fz14"><?= the_field('reliability'); ?></p>
          </div>
          <div class="col-md-3 px-md-3 ">
            <h2 class="cb font-weight-bold text-uppercase m-0">QUALITY</h2>
            <p class="fz12 cl afbf text-uppercase m-0">About us</p>
            <p class=" QS mt-2 fz14"><?= the_field('quality'); ?></p>
          </div>
          <div class="col-md-3 px-md-3 ">
            <h2 class="cb font-weight-bold text-uppercase m-0">excellence</h2>
            <p class="fz12 cl afbf text-uppercase m-0">About us</p>
            <p class=" QS mt-2 fz14"><?= the_field('excellence'); ?></p>
          </div>
        </div>
      </div>
    </section>
    <section class="TM py-5">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h6 class=" cl afbf text-uppercase">about us</h6>
            <h1 class="cb font-weight-bold text-uppercase h1">Our team</h1>
          </div>
        </div>
        <div class="row text-center text-white py-3 pb-5">
          <?php if( have_rows('team') ): ?>
            <?php while ( have_rows('team') ) : the_row(); ?>
							<div class="col2">
		            <div class="img team-data">
		              <img src="<?= (get_sub_field( 'image' )) ? get_sub_field('image') : ''; ?>" class="w-100 img1" alt="">
		              <img src="<?php echo get_template_directory_uri( ); ?>/img/team-back.png" class=" img2" alt="">
		            </div>
		            <div class="txt d-flex flex-column justify-content-center">
		              <p class="font-weight-bold m-0"><?= (get_sub_field( "name" )) ? get_sub_field("name") : ''; ?></p>
		              <p class="small text-uppercase m-0"><?= (get_sub_field( "job_title" )) ? get_sub_field("job_title") : ''; ?></p>
		            </div>
		          </div>
            <?php endwhile; ?>
          <?php else :
                // no rows found
          endif; ?>
        </div>
      </div>
    </section>
    <section class="VS pt-3">
      <div class="container sbtns pb-4">
        <div class="row ">
          <div class="col-md-6">
            <h6 class=" cl afbf text-uppercase">about us</h6>
            <h1 class="cb font-weight-bold text-uppercase h1">Our videos</h1>
          </div>
          <div class="col-md-6">
            <div class="sbtn d-flex justify-content-end">
                <img src="<?= get_template_directory_uri( ); ?>/img/left-arrow.png" alt="" id="al" class="px-3 mt-3">
                <img src="<?= get_template_directory_uri( ); ?>/img/right-arrow.png" alt="" id="ar" class="px-3 mt-3">
            </div>
          </div>
        </div>
      </div>
      <div class="yb">
        <img src="<?= get_template_directory_uri( ); ?>/img/yellow-bar.png" class="img" alt="">
      </div>
      <div class="WS">
        <div class="row m-0 text-white WE pb-4">
					<?php
            if( have_rows('videos') ):
              while ( have_rows('videos') ) : the_row(); ?>
							<?php $youtube_id = ($video_url = get_sub_field( "video_url" )) ? getYouTubeID($video_url) : 'a3ICNMQW7Ok'; ?>
			          <div class="col-md-5 p-0 all_video_url" data-youtube_id="<?php echo $youtube_id ?>">
			            <a href="#" data-toggle="modal" data-target="#modal1" >
			              <div class="vid px-5 py-5 text-white ">
			                <div class="txt py-3">
			                  <img src="<?= get_template_directory_uri( ); ?>/img/play-button.png" alt="">
			                  <h3 class="fwb text-uppercase mt-2"><?php the_sub_field('video_name'); ?></h3>
			                </div>
			              </div>
			            </a>
			          </div>
								<?php
							endwhile;
						else :
							// no rows found
						endif;	?>
        </div>
      </div>
    </section>
		<?php $history = get_field('history'); ?>
		<?php if($history) { ?>
	    <section class="HS bgb py-4">
	      <div class="container py-5">
	        <div class="row py-5 HS__inner">
	          <div class="col-lg-6 px-5">
							<?php if( $history['left_side']['image'] ) { ?>
	            	<img src="<?= $history['left_side']['image'] ?>" class="img-fluid" alt="">
							<?php } ?>
	          </div>
	          <div class="col-lg-6 text-white mt-4 mt-lg-0">
							<?php if( $history['right_side'] ) { ?>
								<?php if( $history['right_side']['title'] ) { ?>
		            <h6 class=" cl afbf text-uppercase">about us</h6>
		            <h2 class=" font-weight-bold text-uppercase h1"><?= $history['right_side']['title'] ?></h2>
								<?php } ?>
                <?php if( $history['right_side']['intro'] ) { ?>
									<h5 class="fwm"><?php echo $history['right_side']['intro'] ?></h5>
		            <?php } ?>
								<?php if( $history['right_side']['description'] ) { ?>
									<p class="QS fz14 mt-3"><?php echo $history['right_side']['description'] ?></p>
		            <?php } ?>
								<?php if( $history['right_side']['button']['button_text'] ) { ?>
									<?php if( $history['right_side']['button']['button_url'] ) { ?>
										<a href="<?php echo $history['right_side']['button']['button_url']; ?>" class=" cb bnt py-3 pr-4 pl-3 text-white justify-content-between d-flex mt-4 QS">
			              	<span><?php echo $history['right_side']['button']['button_text']; ?></span>
			              	<span><img src="<?= get_template_directory_uri( ); ?>/img/btn-arrow-black.png" alt=""></span>
			            	</a>
									<?php } ?>
								<?php } ?>
							<?php } ?>
	          </div>
	        </div>
	        <img src="<?= get_template_directory_uri( ); ?>/img/hs-circle.png" class="img" alt="">
	      </div>
	    </section>
		<?php } ?>
    <div class="modal videoModal" id="modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content border-0 bgt">
          <div class="modal-header border-0 text-center bgt">
            <button type="button" class="close text-white " data-dismiss="modal" aria-label="Close">
              <i class="fa fa-times text-white"></i>
            </button>
          </div>
          <div class="modal-body bgt" id="yt-player">
            <div class="embed-responsive embed-responsive-16by9" >
							<?php $video_id = ($video_link = get_field( "corporate_video" )) ? getYouTubeID($video_link) : 'a3ICNMQW7Ok'; ?>
              <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $video_id; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal videoModal" id="modal1" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content border-0 bgt">
          <div class="modal-header border-0 text-center bgt">
            <button type="button" class="close text-white " data-dismiss="modal" aria-label="Close">
              <i class="fa fa-times text-white"></i>
            </button>
          </div>
          <div class="modal-body bgt three_video" id="yt-player">
            <div class="embed-responsive embed-responsive-16by9" >
              <iframe class="embed-responsive-item" src="" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php
get_footer();
