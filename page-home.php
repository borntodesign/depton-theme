<?php
//
// Template Name: Home
//
?>
<?php get_header(); ?>
<?php $home_header_banner = get_field('home_header_banner');
?>
<?php $background_video_id = ( $background_video_link = $home_header_banner['background_image'] ) ? getYouTubeID($background_video_link) : 'a3ICNMQW7Ok'; ?>
<section class="fading-vid">
  <div class="heroOverlay"></div>
  <div class="fading-vid__close"></div>
	<div class="cover hero py-5 px-2" id ="home-banner" data-id="<?= $background_video_id; ?>">
		<div id="overlay"></div>
	  <div class="hi">
			<div class="container py-5">
			<div class="row py-5">
				<div class="col-12 text-center py-5">
					<div class="py-lg-5">
						<?php	if( $home_header_banner['banner_text'] ) : ?>
						<h1 class="text-uppercase font-weight-bold text-white"><?= $home_header_banner['banner_text']; ?></h1>
						<?php endif ?>
						<?php	if( $home_header_banner['banner_button']['text'] ) : ?>
							<?php $video_id = ( $video_link = $home_header_banner['banner_button']['url'] ) ? getYouTubeID($video_link) : 'a3ICNMQW7Ok'; ?>
							<div class="btn text-white d-flex justify-content-between d-block px-4 py-3 mx-auto mt-5 QS openVideo">
								<span><?= $home_header_banner['banner_button']['text']; ?></span>
								<i class="fa fa-play-circle-o "></i>
							</div>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="tv">
    <div class="tv__overlay"></div>
	  <div class="screen mute" id="tv"></div>
	</div>
</section>
<?php $section_a = get_field('section_a'); ?>
  <section class="UH mt-md-5">
    <div class="row m-0">
      <div class="col-lg-6 px-5 px-2  py-5 lf">
        <div class="px-md-4">
					<?php if( $section_a['left_side']['text'] ) :?>
          <p class="h3 mt-0"><?= $section_a['left_side']['text']; ?></p>
					<?php endif ?>
					<?php if( $section_a['left_side']['button']['text'] ) :?>
						<a href="<?= $section_a['left_side']['button']['link']; ?>" class="bnt py-3 pr-4 pl-3 text-white justify-content-between d-flex mt-5 QS">
              <span><?= $section_a['left_side']['button']['text']; ?></span>
              <span><img src="<?= get_template_directory_uri( ); ?>/img/btn-arrow.png" alt=""></span>
            </a>
					<?php endif ?>
					<?php if( $section_a['left_side']['four_images']) :?>
						<a href="<?= $section_a['left_side']['image_edit']['link']; ?>" class=" mt-5 d-block">
              <img src="<?= $section_a['left_side']['image_edit']['image']; ?>" class=" mt-5 pr-lg-5" alt="">
            </a>
					<?php endif ?>
          <div class="background_image_home" style="background-image:url(<?= get_template_directory_uri( ); ?>/img/svg/home_proc.svg)">
            <?php if( $section_a['left_side']['four_images'] > 0 ) :  ?>
              <div class="home-a-row" id="home-a-row-1">
                <?php
                  $SVGNo = 0;
                  $imageSVG = $section_a['left_side']['four_images'][$SVGNo]['image'];
                ?>
                <a href="#process1"  id="first-image">
                  <?php include(locate_template('template-parts/svg-image.php')); ?>
                </a>
                <?php
                  $SVGNo = 1;
                  $imageSVG = $section_a['left_side']['four_images'][$SVGNo]['image'];
                ?>
                <a href="#process2"  id="second-image">
                  <?php include(locate_template('template-parts/svg-image.php')); ?>
                </a>
              </div>
              <div class="home-a-row" id="home-a-row-2">
                <?php
                  $SVGNo = 2;
                  $imageSVG = $section_a['left_side']['four_images'][$SVGNo]['image'];
                ?>
                <a href="#process3"  id="third-image">
                  <?php include(locate_template('template-parts/svg-image.php')); ?>
                </a>
                <?php
                  $SVGNo = 3;
                  $imageSVG = $section_a['left_side']['four_images'][$SVGNo]['image'];
                ?>
                <a href="#process4"  id="fourth-image">
                  <?php include(locate_template('template-parts/svg-image.php')); ?>
                </a>
              </div>
            <?php endif ?>
          </div>

				</div>
      </div>
      <div class="col-lg-6 pr-0 bb1 py-5">
        <div class="py-md-4">
					<?php if( $section_a['right_side']['image'] ) :?>
          	<img src="<?= $section_a['right_side']['image']; ?>" class="img-fluid mt-4 mt-lg-0 depton-shoe" alt="">
					<?php endif ?>
        </div>
      </div>
    </div>
  </section>

<?php $section_a = get_field('section_b'); ?>
  <section class="CT slide-depton" data-background="#ffffff">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-6 py-4">
          <div class="txt ml-lg-5">
            <p class="small text-uppercase cb fwm px-4 py-1">Annual Revenue</p>
            <h1 class="fwb fig-number" data-counterup-time="1500"><?= ( $section_a['annual_revenue'] ) ? $section_a['annual_revenue'] : 0 ; ?></h1>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 py-4">
          <div class="txt ml-lg-5">
            <p class="small text-uppercase cb fwm px-4 py-1">Happy Customers</p>
            <h1 class="fwb fig-number" data-counterup-time="1500"><?= ( $section_a['happy_customers'] ) ? $section_a['happy_customers'] : 0 ; ?></h1>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 py-4">
          <div class="txt ml-lg-5">
            <p class="small text-uppercase cb fwm px-4 py-1">Countries</p>
            <h1 class="fwb fig-number" data-counterup-time="1500"><?= ( $section_a['countries'] ) ? $section_a['countries'] : 0 ; ?></h1>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 py-4">
          <div class="txt ml-lg-5">
            <p class="small text-uppercase cb fwm px-4 py-1">Pairs of Shoes</p>
            <h1 class="fwb fig-number" data-counterup-time="1500"><?= ( $section_a['pairs_of_shoes'] ) ? $section_a['pairs_of_shoes'] : 0 ; ?></h1>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 py-4">
          <div class="txt ml-lg-5">
            <p class="small text-uppercase cb fwm px-4 py-1">Shoes Per Day</p>
            <h1 class="fwb"><span class="fig-number" data-counterup-time="1500">300</span>-<span class="fig-number" data-counterup-time="1500">3000</span></h1>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 py-4">
          <div class="txt ml-lg-5">
            <p class="small text-uppercase cb fwm px-4 py-1">Customer Service</p>
            <h1 class="fwb"><span class="fig-number" data-counterup-time="1500">24</span>/<span class="fig-number" data-counterup-time="1500">7</span></h1>
          </div>
        </div>
      </div>
    </div>
    <div class="spikes"></div>
  </section>

<?php $consulation = get_field('consulation'); ?>
  <section class="CON" id="process1">
    <div class="scrollArea">
    <!--svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100.6 107.6">
      <path fill="none" stroke="#979797" stroke-width="2" id="star-path" d="M43.7,65.8L19.9,83.3c-2.9,1.9-5.1,3.2-7.9,3.2c-5.7,0-10.5-5.1-10.5-10.8
        c0-4.8,3.8-8.2,7.3-9.8l27.9-12L8.8,41.4c-3.8-1.6-7.3-5.1-7.3-9.8c0-5.7,5.1-10.5,10.8-10.5c2.9,0,4.8,1,7.6,3.2l23.8,17.4
        l-3.2-28.2c-1-6.7,3.5-12,9.8-12c6.3,0,10.8,5.1,9.8,11.7L57,41.8l23.8-17.4c2.9-2.2,5.1-3.2,7.9-3.2c5.7,0,10.5,4.8,10.5,10.5
        c0,5.1-3.5,8.2-7.3,9.8L63.9,53.8l27.9,12c3.8,1.6,7.3,5.1,7.3,10.1c0,5.7-5.1,10.5-10.8,10.5c-2.5,0-4.8-1.3-7.6-3.2L57,65.8
        l3.2,28.2c1,6.7-3.5,12-9.8,12c-6.3,0-10.8-5.1-9.8-11.7L43.7,65.8z"/>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100.6 107.6">
      <path fill="none" stroke="red" stroke-width="2" id="star-path" d="M43.7,65.8L19.9,83.3c-2.9,1.9-5.1,3.2-7.9,3.2c-5.7,0-10.5-5.1-10.5-10.8
        c0-4.8,3.8-8.2,7.3-9.8l27.9-12L8.8,41.4c-3.8-1.6-7.3-5.1-7.3-9.8c0-5.7,5.1-10.5,10.8-10.5c2.9,0,4.8,1,7.6,3.2l23.8,17.4
        l-3.2-28.2c-1-6.7,3.5-12,9.8-12c6.3,0,10.8,5.1,9.8,11.7L57,41.8l23.8-17.4c2.9-2.2,5.1-3.2,7.9-3.2c5.7,0,10.5,4.8,10.5,10.5
        c0,5.1-3.5,8.2-7.3,9.8L63.9,53.8l27.9,12c3.8,1.6,7.3,5.1,7.3,10.1c0,5.7-5.1,10.5-10.8,10.5c-2.5,0-4.8-1.3-7.6-3.2L57,65.8
        l3.2,28.2c1,6.7-3.5,12-9.8,12c-6.3,0-10.8-5.1-9.8-11.7L43.7,65.8z"/>
    </svg

    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100.6 107.6" id="star-svg">
    	<path fill="none" stroke="red" stroke-width="2" id="star-path" d="M43.7,65.8L19.9,83.3c-2.9,1.9-5.1,3.2-7.9,3.2c-5.7,0-10.5-5.1-10.5-10.8
    		c0-4.8,3.8-8.2,7.3-9.8l27.9-12L8.8,41.4c-3.8-1.6-7.3-5.1-7.3-9.8c0-5.7,5.1-10.5,10.8-10.5c2.9,0,4.8,1,7.6,3.2l23.8,17.4
    		l-3.2-28.2c-1-6.7,3.5-12,9.8-12c6.3,0,10.8,5.1,9.8,11.7L57,41.8l23.8-17.4c2.9-2.2,5.1-3.2,7.9-3.2c5.7,0,10.5,4.8,10.5,10.5
    		c0,5.1-3.5,8.2-7.3,9.8L63.9,53.8l27.9,12c3.8,1.6,7.3,5.1,7.3,10.1c0,5.7-5.1,10.5-10.8,10.5c-2.5,0-4.8-1.3-7.6-3.2L57,65.8
    		l3.2,28.2c1,6.7-3.5,12-9.8,12c-6.3,0-10.8-5.1-9.8-11.7L43.7,65.8z"/>
    </svg>
    <svg id="mySVG">
      <path height="210" width="400" fill="none" stroke="red" stroke-width="3" id="triangle" d="M150 0 L100 50 L200 50 Z" />
      Sorry, your browser does not support inline SVG.
    </svg>
    -->

    <img src="<?= get_template_directory_uri( ); ?>/img/svg/shoe_makingaips01.svg" class="consult-scroll scroll-home-icons">
    <img src="<?= get_template_directory_uri( ); ?>/img/svg/shoe_makingaips02.svg" class="design-scroll scroll-home-icons">
    <img src="<?= get_template_directory_uri( ); ?>/img/svg/shoe_makingaips03.svg" class="manu-scroll scroll-home-icons">
    <img src="<?= get_template_directory_uri( ); ?>/img/svg/shoe_makingaips04.svg" class="delivery-scroll scroll-home-icons">
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-6 px-4 px-lg-5 py-2 py-lg-0">
          <h6 class=" cl afbf text-uppercase">Our process</h6>
					<?php if( $consulation['left_side']['title'] ) :?>
          	<h1 class="cb font-weight-bold text-uppercase h1"><?= $consulation['left_side']['title']; ?></h1>
					<?php endif ?>
          <br>
          <?php if( $consulation['left_side']['description'] ) :?>
          <p class="fwm d"><?= $consulation['left_side']['intro'] ?></p>
          <p class="n QS">
            <?= $consulation['left_side']['description']; ?>
          </p>
          <hr class="md">
					<?php endif ?>
        </div>
        <div class="col-md-6 hide-mobile">
          <?php
            $SVGNo_first = 4;
            $SVGNo_second = 5;
            $imageSVG = $consulation['right_side']['image1'];
            $imageSVG2 = $consulation['right_side']['image2'];
          ?>
					<?php if( $imageSVG ) :?>
            <?php include(locate_template('template-parts/svg-image-consult.php')); ?>
					<?php endif ?>
        </div>
      </div>
    </div>
  </section>

<?php $design = get_field('design'); ?>
<section class="DES slide-depton" data-background="#e9eef1" id="process2">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
				<?php if( $design['left_side']['slider'] > 0 ):	?>
          <div id="slider" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
							<?php $count = 0 ;?>
							<?php foreach( $design['left_side']['slider'] as $slide_src ) : ?>
                <div class="carousel-item <?php echo ($count == 0) ? 'active' : ''; ?>">
                  <?php
                    $SVGNo_first = 14 + $count;
                    $SVGNo_second = 16 + $count;
                    $imageSVG = $slide_src['image_1'];
                    $imageSVG2 = $slide_src['image_2'];
                  ?>
                  <?php if( $imageSVG ) :?>
                    <?php include(locate_template('template-parts/svg-image-design.php')); ?>
                  <?php endif ?>
                </div>
							<?php $count++ ;?>
							<?php endforeach; ?>
              <a class="carousel-control-prev indi indil" href="#slider" role="button" data-slide="prev">
                <img src="<?= get_template_directory_uri( ); ?>/img/left-arrow.png" alt="">
              </a>
              <a class="carousel-control-next indi indir" href="#slider" role="button" data-slide="next">
                <img src="<?= get_template_directory_uri( ); ?>/img/right-arrow.png" alt="">
              </a>
          	</div>
					</div>
				<?php else : ?>
				<?php // no rows found ?>
			 	<?php  endif; ?>
			</div>
      <div class="col-md-6 px-4 px-lg-5 py-2 py-lg-0">
        <h6 class=" cl afbf text-uppercase">Our process</h6>
				<?php if($design['right_side']['title']) : ?>
        	<h1 class="cb font-weight-bold text-uppercase h1"><?= $design['right_side']['title'] ?></h1>
				<?php  endif; ?>
        <br>
        <?php if($design['right_side']['description']) : ?>
        <p class="fwm d"><?= $design['right_side']['intro'] ?></p>
        <p class="n QS"><?= $design['right_side']['description'] ?></p>
        <?php  endif; ?>
				<?php if($design['right_side']['button']['text']) : ?>
					<a href="<?= $design['right_side']['button']['link']; ?>" class="bnt py-3 px-4 text-white justify-content-between d-flex mt-3 bb1">
            <span><?= $design['right_side']['button']['text']; ?></span>
            <span><img src="<?= get_template_directory_uri( ); ?>/img/btn-arrow.png" alt=""></span>
          </a>
				<?php  endif; ?>
      </div>
    </div>
  </div>
</section>

<?php $manufacturing = get_field('manufacturing'); ?>
  <section class="MAN slide-depton" data-background="#eee9f1" id="process3">
    <div class="container">
      <div class="row">
        <div class="col-md-6 px-4 px-lg-5 py-2 py-lg-0">
          <h6 class=" cl afbf text-uppercase">Our process</h6>
					<?php if($manufacturing['left_side']['title']) : ?>
          	<h1 class="cb font-weight-bold text-uppercase h1"> Manufacturing</h1>
					<?php  endif; ?>
          <br>
          <?php if($manufacturing['left_side']['description']) : ?>
          <p class="fwm d"><?= $manufacturing['left_side']['intro'] ?></p>
          <p class="n QS"><?= $manufacturing['left_side']['description'] ?></p>
          <hr class="md">
					<?php  endif; ?>
        </div>
        <div class="col-md-6 hide-mobile">
					<?php if($manufacturing['right_side']['image1']) : ?>
            <?php
              $SVGNo_first = 6;
              $SVGNo_second = 7;
              $imageSVG = $manufacturing['right_side']['image1'];
              $imageSVG2 = $manufacturing['right_side']['image2'];
            ?>
            <?php if( $imageSVG ) :?>
              <?php include(locate_template('template-parts/svg-image-manufacturing.php')); ?>
            <?php endif ?>
					<?php  endif; ?>
        </div>
      </div>
    </div>
  </section>

<?php $testimonial = get_field('testimonial'); ?>
  <section class="REW slide-depton" data-background="#f1e9e9">
    <div class="container">
      <div class="row">
        <div class="col-md-8 re d-sm-flex text-center text-sm-left py-3 p-sm-0">
          <div class="img d-sm-flex flex-column justify-content-center">
            <h2 class="fwb cb mobile_quality">Quality Control</h2>
            <p class="small cl afbf text-uppercase mobile_quality w-100">Our process</p>
						<?php if($testimonial['left_side']['image']) : ?>
            	<img src="<?= $testimonial['left_side']['image'] ?>" class="p-3 mr-md-3 mr-2"  alt="">
						<?php  endif; ?>
          </div>
          <div class="txt d-flex flex-column justify-content-center">
						<?php if($testimonial['left_side']['name']) : ?>
            	<h5 class="fwb cb"><?= $testimonial['left_side']['name'] ; ?></h5>
						<?php  endif; ?>
						<?php if($testimonial['left_side']['testimonial_edit']) : ?>
						<?= $testimonial['left_side']['testimonial_edit'] ; ?>
            <?php endif; ?>
					</div>
        </div>
        <div class="col-md-4 qc text-center text-md-left">
          <h2 class="fwb cb desktop_quality">Quality Control</h2>
          <p class="small cl afbf text-uppercase  desktop_quality">Our process</p>
					<?php if($testimonial['right_side']['signature']) : ?>
          	<img src="<?= $testimonial['right_side']['signature'] ; ?>" class=" d-inline-block" alt="">
					<?php endif; ?>
        </div>
      </div>
    </div>
  </section>

<?php $delivery = get_field('delivery'); ?>
  <section class="DEL slide-depton" data-background="#fffff" id="process4">
    <div class="container">
      <div class="row">
        <div class="col-md-6 px-4 px-lg-5 py-2 py-lg-0">
          <h6 class=" cl afbf text-uppercase">Our process</h6>
					<?php if($delivery['left_side']['title']) : ?>
          	<h1 class="cb font-weight-bold text-uppercase h1"><?= $delivery['left_side']['title'] ; ?></h1>
					<?php endif; ?>
          <br>
          <?php if($delivery['left_side']['description']) : ?>
          <p class="fwm d"><?= $delivery['left_side']['intro'] ?></p>
					<p class="n QS"><?= $delivery['left_side']['description'] ; ?></a>
          <?php endif; ?>
				</div>
        <div class="col-md-6 hide-mobile">
					<?php if($delivery['right_side']['image1']) : ?>
            <?php
              $SVGNo_first = 6;
              $SVGNo_second = 7;
              $imageSVG = $delivery['right_side']['image1'];
              $imageSVG2 = $delivery['right_side']['image2'];
            ?>
            <?php if( $imageSVG ) :?>
              <?php include(locate_template('template-parts/svg-image-delivery.php')); ?>
            <?php endif ?>
					<?php endif; ?>
        </div>
      </div>
    </div>
  </section>

  <section class="AF">
    <div class="container pt-2">
      <div class="row text-center pt-5 pb-4">
        <div class="col-12 py-5">
          <h4 class="text-uppercase fwm">Start a FREE consultation with us today. <b>No obligations.</b></h4>
          <h4 class="fwm">We would love to have a chat with you about shoes.</h4>
          <br>
          <div class="lnk d-md-flex justify-content-center">
            <p class=" m-0 d-flex justify-content-center flex-column px-4 fwm RD">+86 28 8777 9435</p>
            <p class="fa-3x cb m-0 d-none d-md-block">/</p>
            <p class=" m-0 d-flex justify-content-center flex-column px-4 fwm QS"><a href="#">sales@depton.com.cn</a></p>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php	if( $video_id ) : ?>
  <div class="modal videoModal" id="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header border-0 text-center bgb">
          <button type="button" class="close text-white " data-dismiss="modal" aria-label="Close">
            <i class="fa fa-times text-white"></i>
          </button>
        </div>
        <div class="modal-body bgb" id="yt-player">
          <div class="embed-responsive embed-responsive-16by9" >
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?= $video_id  ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif ?>

<?php get_footer(); ?>
