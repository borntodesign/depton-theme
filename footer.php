<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package depton
 */

?>
</div><!-- #content -->
<footer>
	<div class="container-fluid  QS">
		<div class="row">
			<div class="col-lg-8 lft py-5 pl-md-5">
				<div class="row">
					<div class="col-md-5 text-white ">
						<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
							<?php dynamic_sidebar( 'footer-1' ); ?>
					 <?php endif; ?>
					</div>
					<div class="col-md-7 mt-4 mt-md-0 pl-md-0">
						<div class="row">
							<div class="col-5 pl-md-0">
								<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
									<?php dynamic_sidebar( 'footer-2' ); ?>
								<?php endif; ?>
							</div>
							<div class="col-7">
								<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
									<?php dynamic_sidebar( 'footer-3' ); ?>
							 <?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 rght py-5 pr-md-5">
				<?php if ( is_active_sidebar( 'footer-4' ) ) : ?>
					<?php dynamic_sidebar( 'footer-4' ); ?>
			 <?php endif; ?>
			</div>
		</div>
	</div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/cda29a36d1.js"></script>
<script src="<?= get_template_directory_uri( ); ?>/js/ajax-custom.js"></script>

<?php wp_footer(); ?>
<script style="font-size: 16px;">
	$(function($, win) {
		$.fn.inViewport = function(cb) {
			return this.each(function(i,el){
				function visPx(){
					var H = $(this).height(),
							r = el.getBoundingClientRect(), t=r.top, b=r.bottom;
					return cb.call(el, Math.max(0, t>0? H-t : (b<H?b:H)));
				} visPx();
				$(win).on("resize scroll", visPx);
			});
		};
	}(jQuery, window));

	(function($) {
		$('.box').hide();
	})( jQuery );
	// $('.quest > p').hide();
</script>
<script>
	jQuery('.head').click(function(event) {
		event.preventDefault();
		jQuery(this).toggleClass('active');
		jQuery(this).children('.h.fa.up').toggleClass('sr-only');;
		jQuery(this).children('.h.fa.down').toggleClass('sr-only');;
		jQuery(this).siblings('.box').toggle('fast');
	});
</script>
<?php if ( get_page_template_slug() == 'page-home.php' ) : ?>
<script>
$( window ).ready(function() {

var wHeight = $(window).height();

$('.slide-depton')
	//.height(wHeight)
	.scrollie({
	scrollOffset : -50,
	scrollingInView : function(elem) {
		var bgColor = elem.data('background');
		$('body').css('background-color', bgColor);
	}
	});
});

var _id = document.getElementById("home-banner");
var data_id = _id.getAttribute('data-id');
var tag = document.createElement('script');
	tag.src = 'https://www.youtube.com/player_api';
var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

if(!isMobileDevice()) {
	var tv,
	playerDefaults = {
		//autoplay: 1,
		autohide: 1,
		modestbranding: 0,
		rel: 0,
		showinfo: 0,
		controls: 0,
		disablekb: 1,
		enablejsapi: 1,
		iv_load_policy: 3,
		loop: 1,
		origin: 'http://depton.samtoms.tech/',
	};
} else {
	var tv,
	playerDefaults = {
		fs: 0,
		modestbranding: 0,
		rel: 0,
		showinfo: 0,
		controls: 0,
		iv_load_policy: 3,
		enablejsapi: 1,
		origin: 'http://depton.samtoms.tech/',
	};
}

var vid = [
	{'videoId': data_id, 'startSeconds': 1, 'endSeconds': 240, 'suggestedQuality': 'hd720'}
],
randomVid = Math.floor(Math.random() * vid.length),
currVid = randomVid;

$('.hi em:last-of-type').html(vid.length);

function isMobileDevice() {
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};

function onYouTubePlayerAPIReady(){
  tv = new YT.Player('tv', {
		events: {
			'onReady': onPlayerReady,
			'onStateChange': onPlayerStateChange
		},
		playerVars: playerDefaults
	});
}

function onPlayerReady(){
  tv.loadVideoById(vid[currVid]);
  tv.mute();
	$('#tv').addClass('mute');
}

function onPlayerStateChange(e) {
  if (e.data === 1){
		if(!isMobileDevice()) {
    	$('#tv').addClass('active');
    	$('.hi em:nth-of-type(2)').html(currVid + 1);
		}
  } else if (e.data === 2){
		if(!isMobileDevice()) {
			$('#tv').removeClass('active');
			if(currVid === vid.length - 1){
				currVid = 0;
			} else {
				currVid++;
			}
			tv.loadVideoById(vid[currVid]);
			tv.seekTo(vid[currVid].startSeconds);
		}
  }
}

function vidRescale(){

  var w = $(window).width(),
    h = $(window).height();
	
  tv.setSize('100%', '100%');
  $('.tv .screen').css({'left': '0px'});
}

$(window).on('load resize', function(){
	vidRescale();
});

$('.tv__overlay').on('click', function(){
	tv.playVideo();
});

$('.openVideo').on('click', function(){
	/* My extra changes */
	$('body').toggleClass('play');
	$('.fading-vid').css('height', $( window ).height());
	setTimeout(() => {
		$('body').toggleClass('play--hide');
	}, 350);
	/* End */
	tv.seekTo(vid[currVid].startSeconds);
	tv.unMute();
	$('#tv').removeClass('mute');
	if(isMobileDevice()){
		$('#tv').addClass('active');
	}

  $('.hi em:first-of-type').toggleClass('hidden');
});

$('.fading-vid__close').on('click', function(){
	tv.mute();
	$('#tv').addClass('mute');

	if(isMobileDevice()){
		$('#tv').removeClass('active');
	}

	$('body').toggleClass('play--hide');
	$('.fading-vid').css('height', '');
	setTimeout(() => {
		$('body').toggleClass('play');
	},1);
});

$('.hi span:last-of-type').on('click', function(){
  $('.hi em:nth-of-type(2)').html('~');
	if(!isMobileDevice()) {
  	tv.pauseVideo();
	}
});

$(".fig-number").counterUp();
</script>
<script>
(function($) {
	$(document).ready(function(){
		$(".background_image_home").on({
			mouseenter: function(){
				$(".home-a-row #first-image svg").animate({left: '10px',bottom: '55px'});
				$(".home-a-row #second-image svg").animate({left: '30px',bottom: '55px'});
				$(".home-a-row #third-image svg").animate({ left: '55px',bottom: '55px'});
				$(".home-a-row #fourth-image svg").animate({ left: '68px',bottom: '55px'});
			},
			mouseleave: function(){
				$(".home-a-row #first-image svg").animate({left: '15px', bottom: '5px'});
				$(".home-a-row #second-image svg").animate({left: '15px', bottom: '15px'});
				$(".home-a-row #third-image svg").animate({left: '66px',bottom: '75px'});
				$(".home-a-row #fourth-image svg").animate({left: '62px',bottom: '75px'});
			}
		});
	});

	window.sr = ScrollReveal({ reset: true });

	// Customizing a reveal set
	sr.reveal('.consult-scroll', { duration: 2000, opacity: 0, scale: 0.2 });
	sr.reveal('.design-scroll', { duration: 2000, opacity: 0, scale: 0.2 });
	sr.reveal('.manu-scroll', { duration: 2000, opacity: 0, scale: 0.2 });
	sr.reveal('.delivery-scroll', { duration: 2000, opacity: 0, scale: 0.2 });
})(jQuery);
</script>
<?php endif; ?>

<?php if ( get_page_template_slug() == 'page-about.php' ) : ?>
<script>
(function($) {
	$(document).ready(function(){
		if($(window).width() < 480) {
			$(".full_hexgon_about #top_hexgen_about").animate({bottom: '122px'});
			$(".full_hexgon_about #bottom_hexgen_about").animate({top: '267px'});
			$(".full_hexgon_about #left_hexgen_about").animate({right: '130px'});
			$(".full_hexgon_about #right_hexgen_about").animate({left: '124px'});
		}
		if($(window).width() > 481 && $(window).width() < 568) {
			$(".full_hexgon_about #top_hexgen_about").animate({bottom: '126px'});
			$(".full_hexgon_about #bottom_hexgen_about").animate({top: '296px'});
			$(".full_hexgon_about #left_hexgen_about").animate({right: '130px'});
			$(".full_hexgon_about #right_hexgen_about").animate({left: '124px'});
		}
		if($(window).width() > 568 && $(window).width() < 768) {
			$(".full_hexgon_about #top_hexgen_about").animate({bottom: '156px'});
			$(".full_hexgon_about #bottom_hexgen_about").animate({top: '326px'});
			$(".full_hexgon_about #left_hexgen_about").animate({right: '154px'});
			$(".full_hexgon_about #right_hexgen_about").animate({left: '160px'});
		}
		if($(window).width() > 768 && $(window).width() < 992) {
			$(".full_hexgon_about #top_hexgen_about").animate({bottom: '157px'});
			$(".full_hexgon_about #bottom_hexgen_about").animate({top: '255px'});
			$(".full_hexgon_about #left_hexgen_about").animate({right: '204px'});
			$(".full_hexgon_about #right_hexgen_about").animate({left: '204px'});
		}
		if($(window).width() > 992) {
			$(".full_hexgon_about #top_hexgen_about").animate({bottom: '157px'});
			$(".full_hexgon_about #bottom_hexgen_about").animate({top: '275px'});
			$(".full_hexgon_about #left_hexgen_about").animate({right: '204px'});
			$(".full_hexgon_about #right_hexgen_about").animate({left: '204px'});
		}
		$(".common_hexgen_abot img").css('opacity',1);
	});
})(jQuery);
</script>
<?php endif; ?>

<script>
(function($) {
	/* Smoothscroll */
	$(document).on('click', 'a[href^="#"]', function (event) {
			event.preventDefault();

			$('html, body').animate({
					scrollTop: $($.attr(this, 'href')).offset().top
			}, 500);
	});
})(jQuery);
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
</script>
<script src="<?= get_template_directory_uri( ); ?>/js/custom.js"></script>

</body>
</html>
