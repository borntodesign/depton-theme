<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package depton
 */
?>
<?php get_header(); ?>
<section class="SM py-5">
  <div class="container py-4">
    <div class="row">
      <div class="col-md-8 pr-md-5 px-3">
				<?php while ( have_posts() ) : ?>
					<?php the_post();
						get_template_part( 'template-parts/content', 'single' );
						/*
						the_post_navigation();

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
						*/ ?>
				<?php endwhile; // End of the loop. ?>
			</div>
      <?= get_sidebar(); ?>
    </div>
  </div>
</section>
<?= get_footer(); ?>
