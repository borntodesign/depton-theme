<?php
//
// Template Name: Contact
//

get_header();
?>
<?php $contact = get_field('contact_us'); ?>
<section class="SM pt-5">
	<div class="container py-4 pb-5">
		<div class="row">
			<div class="col-12">
				<p class="text-uppercase m-0 mt-4  mb-0"><span class="cl afbf">contact us</span></p>
        	<h2 class="cb font-weight-bold text-uppercase">get in touch</h2>
			</div>
		</div>
		<div class="row">
      	<div class="col-md-10 py-2">
        		<div class="row justify-content-between">
        			<div class="col-md-5 d-flex py-3">
        				<p class="mr-md-5 mr-3 text-uppercase fwb contactDetails__label">Address</p>
        				<p class="QS contactDetails__details"><?= ($contact['address']) ? $contact['address'] : ''; ?></p>
        			</div>
        			<div class="col-md-6 py-3">
        				<div class="p mt-2">
        					<span class="mr-md-5 mr-3 text-uppercase fwb contactDetails__label">Email</span>
        					<span class="QS contactDetails__details"><?= ($contact['email']) ? $contact['email'] : ''; ?></span>
        				</div>
        				<div class="p mt-2">
        					<span class="mr-md-5 mr-3 text-uppercase fwb contactDetails__label">phone</span>
        					<span class="QS contactDetails__details"><?= ($contact['phone']) ? $contact['phone'] : ''; ?></span>
        				</div>
        				<div class="p mt-2 social">
        					<span class="mr-md-5 mr-3 text-uppercase fwb contactDetails__label">socail</span>
        					<span class="QS contactDetails__details">
        						<ul class="list-inline m-0 d-inline-block">
        							<li class="list-inline-item"><a href="<?= ($contact['socail']['facebook']) ? $contact['socail']['facebook'] : ''; ?>" class="text-white bgb"><i class="fa fa-facebook"></i></a></li>
        							<li class="list-inline-item"><a href="<?= ($contact['socail']['twitter']) ? $contact['socail']['twitter'] : ''; ?>" class="text-white bgb"><i class="fa fa-twitter"></i></a></li>
        						</ul>
        					</span>
        				</div>
        			</div>
        		</div>
      	</div>
      </div>
	</div>

	<div class="container-fluid map">
		<div class="row">
			<div class="col-lg-6 p-0">
				<div id="map" style="width: 100%; height: 100%; min-height: 400px;"></div>
			</div>
			<div class="col-lg-6 p-0 pt-5 d-flex justify-content-between flex-column map-right-col">
				<?php echo do_shortcode('[contact-form-7 id="164" title="Contact form 1" html_class="form-group pt-5 px-3 px-md-5"]');?>
			</div>
		</div>
	</div>
</section>
<?php $map = get_field('map');?>
<?php $api_key = ($map['api_key']) ? $map['api_key'] : ''; ?>
<?php $address = ($map['address']) ? $map['address'] : ''; ?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo $api_key ?>&callback=initMap"
  type="text/javascript"></script>
<script>
var geocoder;
var map;
function initMap(){
	geocoder = new google.maps.Geocoder();
	var latlng = new google.maps.LatLng(-34.397, 150.644);
	var mapOptions = {
		zoom: 13,
		center: latlng,
		styles: [
            {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
            {
              featureType: 'administrative.locality',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'geometry',
              stylers: [{color: '#263c3f'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'labels.text.fill',
              stylers: [{color: '#6b9a76'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry',
              stylers: [{color: '#38414e'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry.stroke',
              stylers: [{color: '#212a37'}]
            },
            {
              featureType: 'road',
              elementType: 'labels.text.fill',
              stylers: [{color: '#9ca5b3'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry',
              stylers: [{color: '#746855'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry.stroke',
              stylers: [{color: '#1f2835'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'labels.text.fill',
              stylers: [{color: '#f3d19c'}]
            },
            {
              featureType: 'transit',
              elementType: 'geometry',
              stylers: [{color: '#2f3948'}]
            },
            {
              featureType: 'transit.station',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'water',
              elementType: 'geometry',
              stylers: [{color: '#17263c'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.fill',
              stylers: [{color: '#515c6d'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.stroke',
              stylers: [{color: '#17263c'}]
            }
          ]
	}
	map = new google.maps.Map(document.getElementById('map'), mapOptions);

	var address = "<?php echo $address; ?>";
	geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == 'OK') {
			map.setCenter(results[0].geometry.location);
			var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location
			});
		} else {
			alert('Geocode was not successful for the following reason: ' + status);
		}
	});
}
</script>

<?php get_footer(); ?>
