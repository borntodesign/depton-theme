<?php
//
// Template Name: Start Ups
//

get_header();
?>

	<section class="hero pt-5" >
      <div class="container pt-5 pb-5">
        <div class="row">
          <div class="col-12 text-center">
            <p class=" cl afbf text-uppercase m-0">startups</p>
            <h1 class="cb font-weight-bold text-uppercase">startup timeline</h1>
          </div>
        </div>
      </div>
    </section>
    <section class="DTL">
      <div class="img d-md-block d-none">
        <img src="<?= get_template_directory_uri( ); ?>/img/hr-line.png"  alt="">
      </div>
      <div class="container py-5">
        <div class="py-5">
          <div class="row txt text-white py-5 justify-content-end">
            <div class="col-md-2 text-center d-md-block d-none">
              <img src="<?= get_template_directory_uri( ); ?>/img/octagon-hd.png" class="oct mt-2" alt="">
            </div>
            <div class="col-md-5">
                <p class="afbf text-uppercase m-0">startup process</p>
                <h2 class="font-weight-bold text-uppercase"><?php the_field('process'); ?></h2>
                <p class="QS mt-4"><?php the_field('process_one_description'); ?></p>
            </div>
          </div>
          <div class="row txt text-white py-5 justify-content-end flex-row-reverse">
            <div class="col-md-2 text-center d-md-block d-none">
              <img src="<?= get_template_directory_uri( ); ?>/img/octagon-hd.png" class="oct mt-2" alt="">
            </div>
            <div class="col-md-5 text-right">
                <p class="afbf text-uppercase m-0">startup process</p>
                <h2 class="font-weight-bold text-uppercase"><?php the_field('process_two_title'); ?></h2>
                <p class="QS mt-4"><?php the_field('process_two_description'); ?></p>
            </div>
          </div>
          <div class="row txt text-white py-5 justify-content-end">
            <div class="col-md-2 text-center d-md-block d-none">
              <img src="<?= get_template_directory_uri( ); ?>/img/octagon-hd.png" class="oct mt-2" alt="">
            </div>
            <div class="col-md-5">
                <p class="afbf text-uppercase m-0">startup process</p>
                <h2 class="font-weight-bold text-uppercase"><?php the_field('process_three_title'); ?></h2>
                <p class="QS mt-4"><?php the_field('process_three_description'); ?></p>
            </div>
          </div>
          <div class="row txt text-white py-5 justify-content-end flex-row-reverse">
            <div class="col-md-2 text-center d-md-block d-none">
              <img src="<?= get_template_directory_uri( ); ?>/img/octagon-hd.png" class="oct mt-2" alt="">
            </div>
            <div class="col-md-5 text-right">
                <p class="afbf text-uppercase m-0">startup process</p>
                <h2 class="font-weight-bold text-uppercase"><?php the_field('process_four_title'); ?></h2>
                <p class="QS mt-4"><?php the_field('process_four_description'); ?></p>
            </div>
          </div>
          <div class="row txt text-white py-5 justify-content-end">
            <div class="col-md-2 text-center d-md-block d-none">
              <img src="<?= get_template_directory_uri( ); ?>/img/octagon-hd.png" class="oct mt-2" alt="">
            </div>
            <div class="col-md-5">
                <p class="afbf text-uppercase m-0">startup process</p>
                <h2 class="font-weight-bold text-uppercase"><?php the_field('process_five_title'); ?></h2>
                <p class="QS mt-4"><?php the_field('process_five_description'); ?></p>
            </div>
          </div>
          <div class="row txt text-white py-5 justify-content-end flex-row-reverse">
            <div class="col-md-2 text-center d-md-block d-none">
              <img src="<?= get_template_directory_uri( ); ?>/img/octagon-hd.png" class="oct mt-2" alt="">
            </div>
            <div class="col-md-5 text-right">
                <p class="afbf text-uppercase m-0">startup process</p>
                <h2 class="font-weight-bold text-uppercase"><?php the_field('process_six_title'); ?></h2>
                <p class="QS mt-4"><?php the_field('process_six_description'); ?></p>
            </div>
          </div>

        </div>
      </div>
    </section>
    <section class="SL">
      <div class="container-fluid">
        <div class="row pt-md-5">
          <div class="col-12 pb-4 pt-md-5">
              <div id="carousel" class="carousel slide pt-5" data-ride="carousel">
                <div class="carousel-inner text-white text-center ">
                <?php

                  // check if the repeater field has rows of data
                  if( have_rows('testimonials') ):
                    $it = 0;
                    // loop through the rows of data
                      while ( have_rows('testimonials') ) : the_row();
                      $it++;
                      ?>
                        <div class="carousel-item <?php echo $it == 1 ? 'active' : null; ?>">
                          <div class="row justify-content-center pt-5 pb-5 ">
                            <div class="col-md-6 col-8">
                              <p class="n"><?php the_sub_field('description'); ?></p>
                              <h4 class="fwb text-uppercase mt-4"><?php the_sub_field('name'); ?></h4>
                            </div>
                          </div>
                        </div>
                      <?php
                      endwhile;

                  else :

                      // no rows found

                  endif;

                  ?>
                </div>
                <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                  <img src="<?= get_template_directory_uri( ); ?>/img/right-angle.png" alt="">
                </a>
                <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                  <img src="<?= get_template_directory_uri( ); ?>/img/left-angle.png" alt="">
                </a>
                <ol class="carousel-indicators">
                  <li data-target="#carousel" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel" data-slide-to="1"></li>
                  <li data-target="#carousel" data-slide-to="2"></li>
                </ol>
              </div>
          </div>
        </div>
      </div>
    </section>
    <section class="DG py-5">
      <div class="container py-4">
        <div class="row text-white justify-content-center">
        <?php

          // check if the repeater field has rows of data
          if( have_rows('downloads') ):
            $it = 0;
            // loop through the rows of data
              while ( have_rows('downloads') ) : the_row();
              $file = get_sub_field('file');
              $it++;
              ?>
                <div class="col-md-4 col-sm-6 justify-content-center d-flex ">
                  <a href="<?php echo (is_user_logged_in()) ? $file['url']  : ''; ?>" target="_blank" class="tdn" <?php echo (is_user_logged_in()) ? 'download'  : 'data-toggle="modal" data-target="#myModal"'; ?>>
                    <div class="gd px-3 pt-4 pb-2 m-3 text-white text-center">
                      <p class="small text-uppercase fwm text-center m-0"><?php the_sub_field('name'); ?></p>
                      <img src="<?= get_template_directory_uri( ); ?>/img/diamond.png" class="w-50 mt-4 mx-auto" alt="">
                      <p class="d-flex justify-content-between mt-5 m-0">
                        <span>Download guide</span>
                        <span><img src="<?= get_template_directory_uri( ); ?>/img/btn-arrow.png" alt=""></span>
                      </p>
                    </div>
                  </a>
                </div>
              <?php
              endwhile;

          else :

              // no rows found

          endif;

          ?>
        </div>
        <p class="lead fwm text-center mt-5">Learn why our clients love our service.</p>
        <a href="<?php echo get_site_url(); ?>/products" class="bnt py-3 px-4 text-white justify-content-between d-flex mt-4 mx-auto">
          <span>See product collection</span>
          <span><img src="<?= get_template_directory_uri( ); ?>/img/btn-arrow.png" alt=""></span>
        </a>
      </div>
    </section>


<!-- Registration Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <!--  RegistrationModal content-->
    <div class="modal-content">
      <div class="modal-header">
				<h4 class="modal-title">Registration</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body FRM">
				<div class="row justify-content-center d-flex pb-5">
					<div class=" col-sm-12 col-md-10">
						<div id="message_alert"></div>
						<form action="" method="get" class="form-group">
							<div class="sect1">
								<input type="text" id="reg_user" name="username" class="form-control fwb my-4 pl-4 py-2 cb  fst" placeholder="USERNAME*" required="">
								<input type="text" id="reg_firstname" name="firstname" class="form-control fwb my-4 pl-4 py-2 cb" placeholder="FIRST NAME">
								<input type="text" id="reg_company" name="company" class="form-control fwb my-4 pl-4 py-2 cb" placeholder="COMPANY">
								<a href="" class="bnt py-3 px-4 text-white justify-content-between d-flex mt-4 fadebtn">
									<span>See full range</span>
									<span><img src="<?= get_template_directory_uri( ); ?>/img/btn-arrow.png" alt=""></span>
								</a>

							</div>
							<div class="sect2" >
								<input type="text" id="reg_phone_number" name="phone_number" class="form-control fwb my-4 pl-4 py-2 cb" placeholder="PHONE NUMBER*" required="">
								<input type="text" id="reg_country" name="country" class="form-control fwb my-4 pl-4 py-2 cb" placeholder="COUNTRY*" required="">
								<input type="email" id="reg_email"  name="email" class="form-control fwb my-4 pl-4 py-2 cb" placeholder="E-MAIL ADDRESS*" required="">
								<input type="password" id="reg_password" name="password" class="form-control fwb my-4 pl-4 py-2 cb" placeholder="PASSWORD" required="">
								<input type="password" id="reg_cnf_password" name="password" class="form-control fwb my-4 pl-4 py-2 cb" placeholder="CONFIRM PASSWORD*" required="">
								<button type="submit" class="bnt py-3 px-4 text-white justify-content-between d-flex mt-4" id="register_pro" name="register">
									<span>Finish Registration</span>
									<span><img src="<?= get_template_directory_uri( ); ?>/img/btn-arrow.png" alt=""></span>
                </button>
                <div class="recaptcha-container">
								  <div id="my_re_captacha"></div>
								  <span class="msg-alert"></span>
							  </div>
							</div>
							
						</form>
					</div>
				</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onClick="history.go(0)" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php
get_footer();
