<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package depton
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
  <meta name="format-detection" content="telephone=no">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark py-lg-0 px-4">
  <?php
    $image = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ) , 'full' );
    $logo = ($image[0]) ? $image[0] : get_bloginfo('template_url') ."/img/logo.png";
  ?>
  <a class="navbar-brand pb-lg-4" href="<?php echo esc_url( home_url( '/' ) ); ?>">	<img src="<?php echo $logo; ?>" alt=""> </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <?php if ( has_nav_menu( 'primary' ) ) : ?>
    <?php get_template_part( 'template-parts/navigation/navigation', 'depton' ); ?>
  <?php endif; ?>

  </nav>
</header>
